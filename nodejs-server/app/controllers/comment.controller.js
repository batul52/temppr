const db = require("../models");
const { sentimentAnalysis } = require("../utils/sentimentAnalysis.utils");
const getRating = require("../utils/queries")


const Comment = db.comment;

exports.postComment = (req, res) => {
  if(req.body.comment_text)
  rating = sentimentAnalysis.analyzer(req.body.comment_text)
  else
  rating = null
  Comment.create({
    comment_text: req.body.comment_text,
    star_rating: req.body.star_rating,
    rating:rating,
    prod_id: req.query.prod_id,

    user_id: req.query.user_id,
  })
    .then((data) => {
      getRating.getRating(data.prod_id)
      res.send(data);
    })
    
    .catch((err) => {
      res.status(500).send({ message: err.message });
    });
};

exports.findAllComments = (req, res) => {
  Comment.findAll({
   
    where: {
      prod_id: req.query.prod_id,
    },
    
    attributes:["prod_id","comment_text", "star_rating", "rating"],
  })
    .then((data) => {
      res.status(200).send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occured while retrieving comments.",
      });
    });
};

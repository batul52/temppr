const db = require("../models");

const getRating = async (prod_id) => {
  const { QueryTypes } = require("sequelize");

  const comment_rating = await db.sequelize.query(
    " select avg(rating) as avg_comment_rating, prod_id  from comment group by prod_id having prod_id = " +
      prod_id +
      ";",
    {
      type: QueryTypes.SELECT,
    }
  );
  const avg_comment_rating = comment_rating[0].avg_comment_rating;
  console.log(avg_comment_rating);

  const star_rating = await db.sequelize.query(
    " select avg(star_rating) as avg_star_rating, prod_id  from comment group by prod_id having prod_id = " +
      prod_id +
      ";",
    {
      type: QueryTypes.SELECT,
    }
  );
  const avg_star_rating = star_rating[0].avg_star_rating;
  console.log(avg_star_rating);

  let overall_rating = 0;
  if (avg_star_rating === null) {
    overall_rating = avg_comment_rating;
  } else if (avg_comment_rating === null) {
    overall_rating = avg_star_rating;
  } else {
    overall_rating = (avg_comment_rating + avg_star_rating) / 2;
  }

  console.log(overall_rating);

  const get_no_of_reviewers = await db.sequelize.query(
    "select count(distinct(user_id)) as no_of_reviewers, prod_id from comment group by prod_id having prod_id=" +
      prod_id +
      ";",
      {
        type: QueryTypes.SELECT,
      }
  );
  console.log(get_no_of_reviewers)
  const no_of_reviewers = get_no_of_reviewers[0].no_of_reviewers;
  console.log(no_of_reviewers)

  const updated_overall_rating = await db.sequelize.query(
    "UPDATE product SET overall_rating= " +
      overall_rating +
      ", no_of_reviewers= " +
      no_of_reviewers +
      " WHERE id = " +
      prod_id +
      ";",
    {
      type: QueryTypes.SELECT,
    }
  );
};

module.exports = {
  getRating,
  
};

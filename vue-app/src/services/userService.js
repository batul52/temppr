import axios from "@/plugins/axios.js";
import store from "@/store";
import router from "@/router";

export async function userLogin(formData) {
  const response = axios.post("/api/auth/login", formData);
  return await response;
}

export async function userSignup(formData) {
  const response = axios.post("/api/auth/signup", formData);
  return await response;
}
export async function userLogout(formData) {
  const response = axios.get("/api/auth/logout").then(() => {
    store.dispatch("logout");
    router.push("/login");
  })
  .catch(error => {
    console.log(error);
    alert("problem in logout");
  });
  return await response;
}

export async function isUserLoggedIn() {
  const response = axios.get("/api/auth/isloggedin");
  return await response;
}
